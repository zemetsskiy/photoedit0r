import sys
from PyQt5 import QtWidgets
from client.windows.authorization import Authorization
from client.windows.list_of_photos import List_of_Photos

from threading import Thread

from server.app import *

from client.server_connector import ServerConnector


if __name__ == "__main__":
    flaskThread = Thread(target=app_.run, daemon=True).start()

    app = QtWidgets.QApplication(sys.argv)
    connector = ServerConnector("http://localhost", 5000)
    window = Authorization(connector)
    window.show()

    if window.exec_() == QtWidgets.QDialog.Accepted:
        uid = getattr(window, 'uid')
        window = List_of_Photos(connector, uid)
        window.show()
        sys.exit(app.exec_())

