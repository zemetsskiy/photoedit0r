from PIL import Image, ImageEnhance
import PhotoEditor.filters as cf


# Константы
CONTRAST_FACTOR_MAX = 1.5
CONTRAST_FACTOR_MIN = 0.5

SHARPNESS_FACTOR_MAX = 3
SHARPNESS_FACTOR_MIN = -1

BRIGHTNESS_FACTOR_MAX = 1.5
BRIGHTNESS_FACTOR_MIN = 0.5

def color_filter(img, filter_name):

    return cf.color_filter(img, filter_name)


def resize(img, width, height):

    return img.resize((width, height))


def brightness(img, val):

    if val > BRIGHTNESS_FACTOR_MAX or val < BRIGHTNESS_FACTOR_MIN:
        raise ValueError("Val should be in range(0-2)")

    enhancer = ImageEnhance.Brightness(img)
    return enhancer.enhance(val)


def contrast(img, val):

    if val > CONTRAST_FACTOR_MAX or val < CONTRAST_FACTOR_MIN:
        raise ValueError("Val should be in range(0,5-1,5)")

    enhancer = ImageEnhance.Contrast(img)
    return enhancer.enhance(val)


def sharpness(img, val):

    if val > SHARPNESS_FACTOR_MAX or val < SHARPNESS_FACTOR_MIN:
        raise ValueError("Val should be in range(0.5-1.5)")

    enhancer = ImageEnhance.Sharpness(img)
    return enhancer.enhance(val)

def open_img(img):
    img.open()
