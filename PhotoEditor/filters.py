class ColorFilters:
    filters = {"sepia": "Sepia", "negative": "Negative", "black_white": "Black & White"}
    SEPIA, NEGATIVE, BLACK_WHITE = filters.keys()

def black_white(img):
    pix = img.load()
    for i in range(img.width):
        for j in range(img.height):
            s = sum(pix[i, j]) // 3
            pix[i, j] = (s, s, s)

def sepia(img):
    pix = img.load()
    for i in range(img.width):
        for j in range(img.height):
            s = sum(pix[i, j]) // 3
            k = 30
            pix[i, j] = (s+k*2, s+k, s)

def negative(img):
    pix = img.load()
    for i in range(img.width):
        for j in range(img.height):
            pix[i, j] = (255 - pix[i, j][0], 255 - pix[i, j][1], 255 - pix[i, j][2])

def color_filter(img, filter):
    img_copy = img.copy()
    if filter == ColorFilters.SEPIA:
        sepia(img_copy)
    elif filter == ColorFilters.NEGATIVE:
        negative(img_copy)
    elif filter == ColorFilters.BLACK_WHITE:
        black_white(img_copy)
    else:
        raise ValueError(f"can't find filter {filter}")

    return img_copy


