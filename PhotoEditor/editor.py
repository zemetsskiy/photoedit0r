import base64
import io
import time

from functools import partial

from PIL import ImageQt
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
from PyQt5.QtGui import *
from PyQt5 import QtWidgets

from server.app import *
from client.server_connector import ServerConnector
from PhotoEditor import filters
from PhotoEditor import sliders


_img_original = None
_img_preview = None

# Константы
THUMB_BORDER_COLOR_ACTIVE = "#3893F4"
THUMB_BORDER_COLOR = "#ccc"
BTN_MIN_WIDTH = 120
ROTATION_BTN_SIZE = (70, 30)
THUMB_SIZE = 120

SLIDER_MIN_VAL = -100
SLIDER_MAX_VAL = 100
SLIDER_DEF_VAL = 0

def get_ratio_height(width, height, w):
    return int( w/width*height )

def get_ratio_width(width, height, h):
    return int( h/height*width )

# Конвертация значения слайдера для библиотеки Pillow
def get_converted_val(user_p1, user_p2, p1, p2, x):

    res = (x - user_p1) / (user_p2 - user_p1)
    return p1 + res * (p2 - p1)

def get_img_with_all_operations():
    
    brightness = operations.brightness
    contrast = operations.contrast
    sharpness = operations.sharpness

    img = _img_preview
    if operations.brightness != 0:
        img = sliders.brightness(img, brightness)

    if operations.contrast != 0:
        img = sliders.contrast(img, contrast)

    if operations.sharpness != 0:
        img = sliders.sharpness(img, sharpness)

    return img


class Operations:
    def __init__(self):
        self.color_filter = None

        self.brightness = 0
        self.sharpness = 0
        self.contrast = 0

    def reset(self):
        self.color_filter = None

        self.brightness = 0
        self.sharpness = 0
        self.contrast = 0

    def has_changes(self):
        return self.color_filter or self.contrast or self.brightness or self.sharpness


operations = Operations()


class AdjustingTab(QWidget):

    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        contrast_lbl = QLabel("Contrast")
        contrast_lbl.setAlignment(Qt.AlignCenter)

        brightness_lbl = QLabel("Brightness")
        brightness_lbl.setAlignment(Qt.AlignCenter)

        sharpness_lbl = QLabel("Sharpness")
        sharpness_lbl.setAlignment(Qt.AlignCenter)

        self.contrast_slider = QSlider(Qt.Horizontal, self)
        self.contrast_slider.setMinimum(SLIDER_MIN_VAL)
        self.contrast_slider.setMaximum(SLIDER_MAX_VAL)
        self.contrast_slider.sliderReleased.connect(self.on_contrast_slider_released)


        self.brightness_slider = QSlider(Qt.Horizontal, self)
        self.brightness_slider.setMinimum(SLIDER_MIN_VAL)
        self.brightness_slider.setMaximum(SLIDER_MAX_VAL)
        self.brightness_slider.sliderReleased.connect(self.on_brightness_slider_released)


        self.sharpness_slider = QSlider(Qt.Horizontal, self)
        self.sharpness_slider.setMinimum(SLIDER_MIN_VAL)
        self.sharpness_slider.setMaximum(SLIDER_MAX_VAL)
        self.sharpness_slider.sliderReleased.connect(self.on_sharpness_slider_released)


        self.main_layout = QVBoxLayout()
        self.main_layout.setAlignment(Qt.AlignCenter)

        self.main_layout.addWidget(contrast_lbl)
        self.main_layout.addWidget(self.contrast_slider)

        self.main_layout.addWidget(brightness_lbl)
        self.main_layout.addWidget(self.brightness_slider)

        self.main_layout.addWidget(sharpness_lbl)
        self.main_layout.addWidget(self.sharpness_slider)

        self.setLayout(self.main_layout)

    def reset_sliders(self):
        self.brightness_slider.setValue(SLIDER_DEF_VAL)
        self.sharpness_slider.setValue(SLIDER_DEF_VAL)
        self.contrast_slider.setValue(SLIDER_DEF_VAL)

    def on_brightness_slider_released(self):

        val = get_converted_val(SLIDER_MIN_VAL, SLIDER_MAX_VAL, sliders.BRIGHTNESS_FACTOR_MIN,
                                sliders.BRIGHTNESS_FACTOR_MAX, self.brightness_slider.value())

        operations.brightness = val

        self.parent.parent.place_preview_img()
    
    def on_sharpness_slider_released(self):

        val = get_converted_val(SLIDER_MIN_VAL, SLIDER_MAX_VAL, sliders.SHARPNESS_FACTOR_MIN,
                                sliders.SHARPNESS_FACTOR_MAX, self.sharpness_slider.value())


        operations.sharpness = val

        self.parent.parent.place_preview_img()

    def on_contrast_slider_released(self):

        val = get_converted_val(SLIDER_MIN_VAL, SLIDER_MAX_VAL, sliders.CONTRAST_FACTOR_MIN,
                                sliders.CONTRAST_FACTOR_MAX, self.contrast_slider.value())

        operations.contrast = val

        self.parent.parent.place_preview_img()


class FiltersTab(QWidget):

    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.main_layout = QHBoxLayout()
        self.main_layout.setAlignment(Qt.AlignCenter)

        self.add_filter_thumb("none")
        for key, val in filters.ColorFilters.filters.items():
            self.add_filter_thumb(key, val)

        self.setLayout(self.main_layout)

    def add_filter_thumb(self, name, title=""):

        thumb_lbl = QLabel()
        thumb_lbl.name = name
        thumb_lbl.setStyleSheet("border:2px solid #ccc;")

        thumb_lbl.setFixedSize(THUMB_SIZE, THUMB_SIZE)
        thumb_lbl.mousePressEvent = partial(self.on_filter_select, name)

        self.main_layout.addWidget(thumb_lbl)

    def on_filter_select(self, filter_name, e):

        global _img_preview
        if filter_name != "none":
            _img_preview = sliders.color_filter(_img_original, filter_name)
        else:
            _img_preview = _img_original.copy()

        operations.color_filter = filter_name

        self.parent.parent.place_preview_img()


class ActionTabs(QTabWidget):

    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.filters_tab = FiltersTab(self)
        self.adjustment_tab = AdjustingTab(self)


        self.addTab(self.filters_tab, "Filters")
        self.addTab(self.adjustment_tab, "Adjusting")

        self.setMaximumHeight(190)


class PhotoEditorGUI(QtWidgets.QDialog):

    def __init__(self, server: ServerConnector):
        super().__init__()

        self.server = server

        self.setMinimumSize(600, 500)
        self.setMaximumSize(900, 900)
        self.setGeometry(600, 600, 600, 600)

        main_layout = QVBoxLayout()

        self.img_lbl = QLabel()
        self.img_lbl.setAlignment(Qt.AlignCenter)
        main_layout.addWidget(self.img_lbl)

        self.action_tabs = ActionTabs(self)
        main_layout.addWidget(self.action_tabs)

        btn_layout = QHBoxLayout()
        btn_layout.setAlignment(Qt.AlignCenter)

        reset_btn = QPushButton("Reset")
        reset_btn.setMinimumWidth(BTN_MIN_WIDTH)
        reset_btn.clicked.connect(self.on_reset)
        reset_btn.setStyleSheet("font-weight:bold;")
        btn_layout.addWidget(reset_btn)

        save_btn = QPushButton("Save")
        save_btn.setMinimumWidth(BTN_MIN_WIDTH)
        save_btn.clicked.connect(self.on_save)
        save_btn.setStyleSheet("font-weight:bold;")
        btn_layout.addWidget(save_btn)

        btn_layout_widget = QWidget()
        btn_layout_widget.setLayout(btn_layout)
        main_layout.addWidget(btn_layout_widget)

        self.setLayout(main_layout)

    def setImage(self, img, name, size, extension, ctime, uid):
        self.name, self.size, self.extension, self.ctime, self.uid = name, size, extension, ctime, uid
        self.setWindowTitle(f'{name}')

        base64_img_bytes = img.encode('utf-8')
        decoded_img = base64.decodebytes(base64_img_bytes)
        pix = QPixmap()
        pix.loadFromData(decoded_img)
        self.img_lbl.setPixmap(pix)
        self.img_lbl.setScaledContents(True)

        self.action_tabs.adjustment_tab.reset_sliders()

        global _img_original
        _img_original = ImageQt.fromqpixmap(pix)

        if _img_original.width < _img_original.height:
            w = THUMB_SIZE
            h = get_ratio_height(_img_original.width, _img_original.height, w)
        else:
            h = THUMB_SIZE
            w = get_ratio_width(_img_original.width, _img_original.height, h)

        img_filter_thumb = sliders.resize(_img_original, w, h)

        global _img_preview
        _img_preview = _img_original.copy()

        for thumb in self.action_tabs.filters_tab.findChildren(QLabel):
            if thumb.name != "none":
                img_filter_preview = sliders.color_filter(img_filter_thumb, thumb.name)
            else:
                img_filter_preview = img_filter_thumb

            preview_pix = ImageQt.toqpixmap(img_filter_preview)
            thumb.setPixmap(preview_pix)

    def on_save(self):

        img = get_img_with_all_operations()

        buffered = io.BytesIO()
        img.save(buffered, format=f"{self.what_format(self.extension)}")
        img_bytes = base64.b64encode(buffered.getvalue())

        new_name, ok = self.get_new_name()

        if ok:
            # Случай когда введено хотя бы что-нибудь
            if new_name != "":
                if self.server.add_photo(img_bytes, new_name, self.uid, self.size, self.extension, time.ctime()):
                    QMessageBox.about(self, "Success", "Edited photo added")

                else:
                    QMessageBox.about(self, "Error", "Try again")
            else:
                if self.server.add_photo(img_bytes, self.name, self.uid, self.size, self.extension, time.ctime()):
                    QMessageBox.about(self, "Success", "Edited photo added")

                else:
                    QMessageBox.about(self, "Error", "Try again")
        else:
            if self.server.add_photo(img_bytes, self.name, self.uid, self.size, self.extension, time.ctime()):
                QMessageBox.about(self, "Success", "Edited photo added")

            else:
                QMessageBox.about(self, "Error", "Try again")

    def on_reset(self):

        global _img_preview
        _img_preview = _img_original.copy()

        operations.reset()

        self.place_preview_img()
        self.action_tabs.adjustment_tab.reset_sliders()

    def place_preview_img(self):
        img = get_img_with_all_operations()

        preview_pix = ImageQt.toqpixmap(img)
        self.img_lbl.setPixmap(preview_pix)

    def what_format(self, format):
        if format == "png": return "PNG"
        elif format == "jpeg" or format == "jpg": return "JPEG"
        else:
            print("Unknown format...")

    def get_new_name(self):
        input = QInputDialog()
        text, ok = input.getText(self.img_lbl, 'Enter new name', "Name: ")
        return text, ok