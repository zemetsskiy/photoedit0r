import requests
import json

class ServerConnector:

    def __init__(self, address, port):
        self.url = f"{address}:{port}"

    def add_user(self, new_username, new_password):
        result = requests.post(f"{self.url}/users",
                               json={
                                   'new_username': new_username,
                                   'new_password': new_password
                               }).json()

        print("сервер коннектор получил обратный ответ")

        if result['status'] == 'ok':
            return True
        else:
            return False

    def auth(self, username, password):
        result = requests.get(f"{self.url}/users",
                               json={
                                   'username': username,
                                   'password': password
                               }).json()

        print("сервер коннектор получил обратный ответ")

        if result['status'] == 'ok':
            return result['uid']
        else:
            return False

    def load_photos(self, uid):
        result = requests.get(f"{self.url}/photos",
                              json={
                                  'uid': uid
                              }).json()

        print("сервер коннектор получил обратный ответ")

        if result['status'] == 'ok':
            return result['data']
        else:
            return False

    def add_photo(self, image, name, uid, size, extension, ctime):
        print("Зашел в коннектор")
        result = requests.post(f"{self.url}/photos",
                              json={
                                  'image': image,
                                  'name': name,
                                  'uid': uid,
                                  'size': size,
                                  'extension': extension,
                                  'ctime': ctime
                              }).json()

        print("сервер коннектор получил обратный ответ")

        if result['status'] == 'ok':
            return True
        else:
            return False

    def delete_photo(self, image, name, uid):
        print("Зашел в коннектор")
        result = requests.post(f"{self.url}/delete_photo",
                              json={
                                  'image': image,
                                  'name': name,
                                  'uid': uid
                              }).json()

        print("сервер коннектор получил обратный ответ")

        if result['status'] == 'ok':
        #if result == True:
            return True
        else:
            return False
