from PyQt5 import QtCore, QtGui, QtWidgets

from PyQt5.QtWidgets import QMessageBox, QFileDialog

from client.server_connector import ServerConnector

from PhotoEditor.editor import PhotoEditorGUI

import base64

COL_NUM = 6


class UI_List_of_Photos(object):
    def __init__(self, server: ServerConnector, uid):
        self.server = server
        self.uid = uid

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("Your photos")
        MainWindow.resize(1000, 696)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setStyleSheet("background-color: rgb(20, 20, 40)")
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(30, 30, 251, 51))
        font = QtGui.QFont()
        font.setFamily("Montserrat-medium")
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label.setStyleSheet("color: white")


        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(30, 100, 900, 371))
        self.tableWidget.setAutoFillBackground(True)
        self.tableWidget.setStyleSheet("color: rgb(180, 180, 180);\n"
                                       "background-color: rgb(34, 40, 64);\n"
                                       "padding: 4px;\n"
                                       "font-size: 18px;\n"
                                       "font-weight: normal;\n"
                                       "font-family: Montserrat-medium;\n"
                                        "border: 0px solid white;\n"
                                       )

        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(COL_NUM)
        self.tableWidget.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        self.tableWidget.setSelectionMode(QtWidgets.QTableWidget.NoSelection)

        self.tableWidget.setHorizontalHeaderLabels(["Name", "Size", "Extension", "Creation time", " ", " "])
        self.tableWidget.setColumnWidth(0, 150)
        self.tableWidget.setColumnWidth(1, 130)
        self.tableWidget.setColumnWidth(2, 130)
        self.tableWidget.setColumnWidth(3, 250)
        self.tableWidget.setColumnWidth(4, 70)
        self.tableWidget.setColumnWidth(5, 70)
        self.tableWidget.verticalHeader().setVisible(False)


        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 26))
        self.menubar.setObjectName("menubar")
        self.menuLoad = QtWidgets.QMenu(self.menubar)
        self.menuLoad.setObjectName("menuLoad")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionLoadFile = QtWidgets.QAction(MainWindow)
        self.actionLoadFile.setObjectName("actionLoadFile")
        self.menuLoad.addAction(self.actionLoadFile)
        self.menubar.addAction(self.menuLoad.menuAction())

        self.refresh_btn = QtWidgets.QPushButton(self.centralwidget)
        self.refresh_btn.clicked.connect(lambda: self.load_list(self.server.load_photos(self.uid)))
        self.refresh_btn.setIcon(QtGui.QIcon("refresh-48.png"))
        self.refresh_btn.setIconSize(QtCore.QSize(24, 24))
        self.refresh_btn.move(220, 40)

        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.move(260, 45)
        font2 = QtGui.QFont()
        font2.setFamily("Montserrat-medium")
        font2.setPointSize(13)
        font2.setBold(True)
        font2.setWeight(70)
        self.label_2.setFont(font2)
        self.label_2.setObjectName("label_2")
        self.label_2.setStyleSheet("color: white")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.window = PhotoEditorGUI(self.server)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("Your Photos", "Your Photos"))
        self.label.setText(_translate("Your Photos", "Your Photos"))
        self.label_2.setText(_translate("Click after adding the modified photo", "Click after adding the modified photo"))
        self.menuLoad.setTitle(_translate("Your Photos", "Load"))
        self.actionLoadFile.setText(_translate("Your Photos", "Load a file"))
        self.actionLoadFile.setShortcut(_translate("Your Photos", "Ctrl+F"))

    def load_list(self, array):

        self.array = array
        self.tableWidget.setRowCount(len(self.array))

        self.tableWidget.doubleClicked.connect(lambda: self.edit_photo(self.array))

        tablerow = 0
        for row in self.array:

            resetButton = QtWidgets.QPushButton()
            resetButton.setText("Delete")
            resetButton.clicked.connect(lambda: self.reset_photo(self.array))
            resetButton.setStyleSheet("color: red;\n"
                                      "padding: 4px;\n"
                                      "font-size: 18px;\n"
                                      "font-weight: 500;\n")

            saveButton = QtWidgets.QPushButton()
            saveButton.setText("Save")
            saveButton.setStyleSheet("color: white;\n"
                                     "padding: 4px;\n"
                                     "font-size: 20px;\n"
                                     "font-weight: 500;\n"
                                     )
            saveButton.clicked.connect(lambda: self.save_photo(self.array))

            for i in range(4):
                self.tableWidget.setItem(tablerow, i, QtWidgets.QTableWidgetItem(str(row[i + 1])))

            self.tableWidget.setCellWidget(tablerow, 4, resetButton)
            self.tableWidget.setCellWidget(tablerow, 5, saveButton)
            tablerow += 1

    def edit_photo(self, array):
        row = self.tableWidget.currentIndex().row()

        if self.window == None:
            self.window = PhotoEditorGUI(self.server)

        self.window.setImage(array[row][0], array[row][1], array[row][2], array[row][3], array[row][4], self.uid)

        self.window.show()

    def reset_photo(self, array):
        row = self.tableWidget.currentIndex().row()
        qm = QMessageBox()

        response = qm.question(self.label, ' ', "Are you sure to reset this photo?", qm.Yes | qm.No)

        if response == qm.Yes:
            if self.server.delete_photo(array[row][0], array[row][1], self.uid):
                qm.information(self.label, ' ', "Photo deleted.")
                self.load_list(self.server.load_photos(self.uid))
            else:
                qm.information(self.label, ' ', "Error, maybe photo is already deleted.")
        else:
            qm.information(self.label, ' ', "Ok, nothing changed.")

    def save_photo(self, array):
        row = self.tableWidget.currentIndex().row()
        name = QFileDialog.getSaveFileName()[0]
        if name:
            with open(name, 'wb') as file:
                base64_img = array[row][0]
                base64_img_bytes = base64_img.encode('utf-8')
                decoded_image_data = base64.decodebytes(base64_img_bytes)

                file.write(decoded_image_data)
                file.close()
                qm = QMessageBox()
                qm.information(self.tableWidget, ' ', "Photo saved !")
        else:
            pass