from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox

from client.server_connector import ServerConnector

from client.windows.design.login_form_design import *


class Authorization(QtWidgets.QDialog):

    def __init__(self, server: ServerConnector, parent=None):
        super().__init__(parent)

        self.server = server

        #self.window = Ui_Authorization()
        self.window = LoginForm()
        self.window.setup_ui(self)
        #self.window.show()

        # Привязываем функции к кнопкам
        self.window.pushButton_2.clicked.connect(self.registration)
        self.window.pushButton.clicked.connect(self.auth)

        self.base_line_edit = [self.window.lineEdit, self.window.lineEdit_2]

    def auth(self):
        username = self.window.lineEdit.text()
        passwd = self.window.lineEdit_2.text()
        self.server.auth(username, passwd)

        if not (self.server.auth(username, passwd) is False):
           self.uid  = self.server.auth(username, passwd)
           self.accept()
        else:
            QMessageBox.about(self, "Error", "Not valid login or password.")

    def registration(self):
        # Вызываем окно регистрации
        new_username = self.window.lineEdit.text()
        new_passwd = self.window.lineEdit_2.text()


        # Реализация post запроса ( добавление в бд )
        if self.server.add_user(new_username, new_passwd):
            QMessageBox.about(self, "Success", "Registration success.")
        else:
            QMessageBox.about(self, "Error", "User with the same name already exists.")
