from client.windows.design.list_of_photos_design import *

from client.server_connector import ServerConnector

import os, base64
import time

class List_of_Photos(QtWidgets.QMainWindow):

    def __init__(self, server: ServerConnector, uid, parent=None):
        super().__init__(parent)

        self.server = server

        self.uid = uid

        self.window = UI_List_of_Photos(self.server, self.uid)
        self.window.setupUi(self)


        # Привязываем к кнопкам менюшки функции
        self.window.actionLoadFile.triggered.connect(lambda: self.add_photo())

        self.load_photos()
    
    def load_photos(self):

        array = self.server.load_photos(self.uid)

        self.window.load_list(array)

    
    def add_photo(self):

        filename = QFileDialog.getOpenFileName(filter="Image (*.*)")[0]
        try:
            with open(filename, 'rb') as binary_file:

                binary_file_data = binary_file.read()

                base64_encoded_data = base64.b64encode(binary_file_data)

                base64_message = base64_encoded_data.decode('utf-8')

                name = os.path.splitext(os.path.basename(filename))[0]
                size = round(os.path.getsize(filename) / 1024)
                extension = os.path.splitext(filename)[1][1:]
                ctime = time.ctime(os.path.getctime(filename))

                if self.server.add_photo(base64_message, name, self.uid, size, extension, ctime):
                    QMessageBox.about(self, "Success", "Photo added")
                    self.load_photos()
                else:
                    QMessageBox.about(self, "Error", "Try again")
        except FileNotFoundError:
            QMessageBox.about(self, "Error", "No photo selected.")
