import sqlite3
import secrets
import base64

from server.sha256 import encrypt_passwd


class UserDb:

    def __init__(self):
        self.conn = sqlite3.connect('users.db', check_same_thread=False)
        self.c = self.conn.cursor()
        self.c.execute('''CREATE TABLE IF NOT EXISTS users(username TEXT NOT NULL, password TEXT NOT NULL, token TEXT NOT NULL)''')
        self.conn.commit()

    def add_user(self, new_username, new_password):
        self.c.execute("SELECT username from users WHERE username = ?", (new_username,))
        if not self.c.fetchone():
            info = (new_username, encrypt_passwd(new_password, new_username), secrets.token_hex(16))
            self.c.execute('''INSERT INTO users(username, password, token) VALUES (?, ?, ?)''',info)
            self.conn.commit()
            print("Успешно добавлен пользователь")
            return True
        else:
            return False

    def auth(self, username, password):
        info = (username, encrypt_passwd(password, username))
        self.c.execute("SELECT username from users WHERE username = ? AND password = ?", info)
        if not self.c.fetchone():
            return False
        else:
            return True

    def get_uid(self, username):
        self.c.execute("SELECT token from users WHERE username = ?", (username,))
        uid = self.c.fetchone()[0]
        self.conn.commit()
        return uid


class PhotosDb:

    def __init__(self):
        self.conn = sqlite3.connect('photos.db', check_same_thread=False)
        self.c = self.conn.cursor()
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS photos(image BLOB, name TEXT NOT NULL, size TEXT NOT NULL, extension TEXT, ctime TEXT NOT NULL, uid TEXT NOT NULL)''')

        self.conn.commit()

    def load_photos(self, uid):
        array = []
        cur = self.c.execute("SELECT * from photos WHERE uid = ?", (uid,))
        for photo in cur.fetchall():
            base64_encoded_data = base64.b64encode(photo[0])
            base64_message = base64_encoded_data.decode('utf-8')

            info = (base64_message, photo[1], photo[2], photo[3], photo[4])
            array.append(info)
        self.conn.commit()
        return array

    def add_photo(self, base64_img, name, uid, size, extension, ctime):

        base64_img_bytes = base64_img.encode('utf-8')
        decoded_image_data = base64.decodebytes(base64_img_bytes)

        info = (decoded_image_data, name, size, extension, ctime, uid)
        if self.c.execute('''INSERT INTO photos(image, name, size, extension, ctime, uid) VALUES (?, ?, ?, ?, ?, ?)''', info):
            self.conn.commit()
            return True
        else:
            return False

    def delete_photo(self, base64_img, name, uid):

        base64_img_bytes = base64_img.encode('utf-8')
        decoded_image_data = base64.decodebytes(base64_img_bytes)

        info = (decoded_image_data, name, uid)
        if self.c.execute('''DELETE FROM photos WHERE image = ? AND name = ? AND uid = ?''', info):
            self.conn.commit()
            return True
        else:
            return False