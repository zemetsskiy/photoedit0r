from flask import Flask, request, json

from server.database import UserDb, PhotosDb


app_ = Flask(__name__)

UserDb = UserDb()
PhotosDb = PhotosDb()

@app_.route('/users', methods=['POST'])
def add_user():
    if not request.is_json:
        return json.dumps({"status": "error"})
    data = request.json
    if UserDb.add_user(data['new_username'], data['new_password']):
        return json.dumps({"status": "ok"})
    else:
        return json.dumps({"status": "error"})


@app_.route('/users', methods=['GET'])
def auth():
    if not request.is_json:
        return json.dumps({"status": "error"})

    data = request.json

    if UserDb.auth(data['username'], data['password']):
        uid = UserDb.get_uid(data['username'])
        return json.dumps({"status": "ok", "uid": uid})
    else:
        return json.dumps({"status": "error"})


@app_.route('/photos', methods=['GET'])
def load_photos():
    if not request.is_json:
        return json.dumps({"status": "error"})
    data = request.json
    array = PhotosDb.load_photos(data['uid'])
    return json.dumps({"status": "ok", "data": array})


@app_.route('/photos', methods=['POST'])
def add_photo():
    if not request.is_json:
        return json.dumps({"status": "error"})
    data = request.json

    if PhotosDb.add_photo(data['image'], data['name'], data['uid'], data['size'], data['extension'], data['ctime']):
        return json.dumps({"status": "ok"})
    else:
        return json.dumps({"status": "error"})


@app_.route('/delete_photo', methods=['POST'])
def delete_photo():
    if not request.is_json:
        return json.dumps({"status": "error"})
    data = request.json

    if PhotosDb.delete_photo(data['image'], data['name'], data['uid']):
        return json.dumps({"status": "ok"})

    else:
        return json.dumps({"status": "error"})
