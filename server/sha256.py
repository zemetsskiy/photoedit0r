import hashlib


def encrypt_passwd(passwd, username):
    usn = hashlib.sha256(username.encode()).hexdigest()
    sha_signature = hashlib.sha256((passwd+usn).encode()).hexdigest()
    return sha_signature
