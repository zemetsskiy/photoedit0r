# PhotoEdit0r



## Implementation of simple photo edit0r
![Screenshot](https://gitlab.com/zemetsskiy/photoedit0r/-/raw/main/auth.png)
![Screenshot](https://gitlab.com/zemetsskiy/photoedit0r/-/raw/main/main.png)
![Screenshot](https://gitlab.com/zemetsskiy/photoedit0r/-/raw/main/editor.png)

### Used libs
- PyQt
- Pillow
- Flask

#### To install all packages use 

```sh
pip3 install -r requirements.txt
```

## Running app instructions

```sh
git clone https://gitlab.com/zemetsskiy/photoedit0r.git
cd photoedit0r

pip3 install pyinstaller
pyinstaller --add-data="icons/*.png:." main.py --onefile

cp icons/* dist
cd dist
./main

```


